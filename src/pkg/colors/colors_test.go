package colors

import (
	"color-generator/constants"
	"fmt"
	"testing"
)

func TestFctColorPosition(t *testing.T) {
	tests := []struct {
		index  int
		result string
	}{
		{0, constants.COLOR_1},
		{1, constants.COLOR_2},
		{2, constants.COLOR_3},
		{3, constants.COLOR_4},
		{4, constants.COLOR_5},
		{5, ""},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("Index %d", test.index), func(t *testing.T) {
			color := FctColorPosition(test.index)
			if color != test.result {
				t.Errorf("Expected %s, but got %s", test.result, color)
			}
		})
	}
}

func TestColorTyp_Luma(t *testing.T) {
	color := ColorTyp{Value: "FF5733", Name: "Red"}

	expectedLuma := 120

	luma := color.Luma()
	if luma != expectedLuma {
		t.Errorf("Expected Luma %d, but got %d", expectedLuma, luma)
	}
}

func TestFctHex2RGB(t *testing.T) {
	tests := []struct {
		hex    string
		result RBGTyp
	}{
		{"FF5733", RBGTyp{Red: 255, Green: 87, Blue: 51}},
		{"00FF00", RBGTyp{Red: 0, Green: 255, Blue: 0}},
		{"0000FF", RBGTyp{Red: 0, Green: 0, Blue: 255}},
	}

	for _, test := range tests {
		t.Run("Hex "+test.hex, func(t *testing.T) {
			rgb, err := FctHex2RGB(test.hex)
			if err != nil {
				t.Errorf("Expected no error, but got %v", err)
			}
			if rgb != test.result {
				t.Errorf("Expected %v, but got %v", test.result, rgb)
			}
		})
	}
}

func TestFctGetColor(t *testing.T) {
	tests := []struct {
		colorIndex int
		result     ColorTyp
	}{
		{0, ColorTyp{Value: "000000", Name: "Black"}},
		{-255000, ColorTyp{Value: "", Name: ""}},
	}

	for _, test := range tests {
		t.Run(
			fmt.Sprintf("Color index %d", test.colorIndex), func(t *testing.T) {
				color := FctGetColor(test.colorIndex)
				if color != test.result {
					t.Errorf("Expected %v, but got %v", test.result, color)
				}
			})
	}
}
