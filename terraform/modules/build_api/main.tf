terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "color_generator" {
  name         = "${var.namespace}/${var.project_name}"
  build {
    context = "../"
    dockerfile = "../Dockerfile"
  }
}

output "docker_image" {
  value = docker_image.color_generator.name
}